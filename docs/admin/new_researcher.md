# New Researcher
Researchers who have not registered to use the UK Biobank resource can register using the [Access Management System.](https://bbams.ndph.ox.ac.uk/ams/)

You will be expected to provide the following information:

1. Your name, address, email and telephone number
2. Your curriculum vitae
3. A list of your peer-reviewed publications (with links to the papers whenever possible)
4. Details of any complaints against you within the previous three years
5. Information about your institution (i.e. name of your research department, address, telephone and website)

UK Biobank Coordinating Centre staff will review the identity and the bona fides of the researcher. If the registration is approved, the researcher will be issued with a unique identifying number (indicating that they are an approved researcher) which should be used for any subsequent application. 

The researcher can then choose to make an application (see following section) and/or be named as a collaborator on an existing application. 
If the researcher has been named as a collaborator on an application to access the UK Biobank resource, they will need to log back into the Access Management System and accept the invitation that will be visible on the researcher's Home Page.
