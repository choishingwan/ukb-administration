# Introduction to UK Biobank
## Introduction

The UK Biobank is a very large population-based prospective study that includes detailed health-related and genetic data on about 500,000 individuals and is available to the research community. Men and women aged 40-69 years were recruited during 2006-2010 across 22 recruitment centers in the United Kingdom (Figure 1). Participants provided general consent for all types of health research by both academic and commercial researchers and follow-up through health-related records.

<center>
<img src="img/ukb_centres.png" alt="ukb_centres" width="300" /> 

Figure 1. The UK Biobank cohort recruitment centres (Source: [UK Biobank](https://biobank.ndph.ox.ac.uk/showcase/exinfo.cgi?src=UKB_centres_map))
</center>

## The aims of the UK Biobank cohort 

- To allow detailed investigations of genetic and non-genetic determinants of disease of middle and old age. UK Biobank provides extensive and precise assessments of exposures with comprehensive follow up and characterization of many different health-related outcomes.


- To promote innovative science by maximizing access to the resource. UK Biobank is open to bona fide researchers anywhere in the world, including those funded by academia and industry.

!!! note 
	The participants age range in the UK Biobank cohort was based on compromise to get people without a disease, and to get significant health outcomes during early years of follow up. This prospective approach enables the measurement of risk factors before the disease develops, and therefore 1) avoids reverse causality and recall bias, 2) improves measurement detail, 3) reduces measurement error. 

## Participants demographics

- 	46% male
-	57% aged 40-59 years; 43% aged 60-69 years
-	*Less socioeconomically deprived than UK average*, but all strata represented
-	85% urban
-	94.5% white; 5.5% other
-	58% paid employment/ self employed
-	89% recruited in England; 7% in Scotland; 4% in Wales


## Current and planned data for UK Biobank

We encourage to visit [the official UK Biobank website](https://biobank.ctsu.ox.ac.uk/crystal/exinfo.cgi?src=timelines_all) for the most updated information and timelines about data availability. Please note number of participants in table below are approximate and may change as further updates on the cohort data recruitment become available *(Last update: November 2020)*. 

### UK BIOBANK ASSESSMENT CENTRE
 <table class="others">
  <tr>
    <th></th>
	<th><b>NUMBER OF PARTICIPANTS</b></th>
    <th><b>DETAILS</b></th>
    <th><b>DATE OF DATA ACQUISITION</b></th>
	<th><b>DATA FIRST AVAILABLE FOR RESEARCH</b></th>
  </tr>
  <tr>
    <td>UKB Baseline assessment</td>
    <td>Whole cohort</td>
    <td> 
		<ul>
  			<li>
			  Socio-demographics and lifestyle factors collected with touchscreen questionnaire and brief verbal interview.
			</li>
  			<li>
			  Physical measurements (blood pressure, arterial stiffness, eye measures, body composition, hand-grip strength, ECG, etc).
			</li>
  			<li>
			  Collection of blood, urine and saliva samples
			</li>
			<a href="https://biobank.ndph.ox.ac.uk/showcase/exinfo.cgi?src=baseline_data">Link to UKB.</a>
		</ul>
	</td>
	<td>2006-2010</td>
	<td>Q2 2012</td>
  </tr>
  <tr>
    <td>Repeat of baseline assessment</td>
    <td>20,000 - 25,000</td>
    <td> 
		Same as above. <a href="https://biobank.ctsu.ox.ac.uk/~bbdatan/Repeat_assessment_doc_v1.0.pdf">Link to UKB.</a>
	<td>2012-2013</td>
	<td>Q3 2013</td>
  </tr>
  </table>
  <br/>

### ONLINE FOLLOW-UP
 <table class="others">
  <tr>
    <th></th>
	<th><b>NUMBER OF PARTICIPANTS</b></th>
    <th><b>DETAILS</b></th>
    <th><b>DATE OF DATA ACQUISITION</b></th>
	<th><b>DATA FIRST AVAILABLE FOR RESEARCH</b></th>
  </tr>
  <tr>
    <td>Online 24-h dietary recall web questionnaire</td>
    <td>210,000</td>
    <td> 
		Detailed questions on the intake of foods and beverages consumed during the previous 24-hour period. <a href="https://biobank.ctsu.ox.ac.uk/crystal/crystal/docs/DietWebQ.pdf">Link to UKB.</a>
	</td>
	<td>2011-2012</td>
	<td>Q2 2013</td>
  </tr>
  <tr>
    <td>Digestive health</td>
    <td>~180,000</td>
    <td> 
		Questionnaire with self-reported information on abdominal and associated symptoms for the study of Irritable bowel syndrome and related disorders. <a href="https://biobank.ctsu.ox.ac.uk/crystal/crystal/docs/digestive_health.pdf">Link to UKB.</a>
	<td>2017</td>
	<td>2018</td>
  </tr>
  <tr>
    <td>Food (and other) preferences</td>
    <td>~180,000</td>
    <td> 
		Questionnaire with items reflecting both sensory preferences (bitter, sweet etc.) and foodstuff preferences (fruit, vegetables, meat, etc.) <a href="https://biobank.ctsu.ox.ac.uk/crystal/crystal/docs/foodpref.pdf">Link to UKB.</a>
	<td>Q4 2019</td>
	<td>Q1 2020</td>
  </tr>
  <tr>
    <td>Physical activity data with accelerometry</td>
    <td>100,000</td>
    <td> 
		Wrist worn tri-axial accelerometer - type, intensity, and duration of PA; one-week test. <a href="https://doi.org/10.1371/journal.pone.0169649">Link to UKB.</a>
	<td>2013-2015</td>
	<td>2015</td>
  </tr>
  <tr>
    <td>Online ‘Healthy Work questionnaire’</td>
    <td>100,000 – 120,000</td>
    <td> 
		Occupational history since finishing full time education; respiratory health outcomes and medication for these conditions; and smoking habits. <a href="https://biobank.ctsu.ox.ac.uk/crystal/crystal/docs/OccupHealthOSCAR.pdf">Link to UKB.</a>
	<td>Q3 2015</td>
	<td>Q2 2017</td>
  </tr>
  <tr>
    <td>Questionnaire on cognitive function</td>
    <td>100,000 – 120,000</td>
    <td> 
		Tests for mood, fluid intelligence, trail making, symbol digit substitution pairs matching, numeric memory. <a href="http://biobank.ctsu.ox.ac.uk/crystal/label.cgi?id=116">Link to UKB.</a>
	<td></td>
	<td></td>
  </tr>
  <tr>
    <td>Questionnaire on mental health</td>
    <td>~160,000</td>
    <td> 
		Questionnaire on life-time experiences of mental disorders. <a href="https://biobank.ctsu.ox.ac.uk/crystal/crystal/docs/mental_health_online.pdf">Link to UKB.</a>
	<td>2016</td>
	<td>Q3 2017</td>
  </tr>
  </table>
   <br/>

### IMAGING
<table class="others">
  <tr>
    <th></th>
	<th><b>NUMBER OF PARTICIPANTS</b></th>
    <th><b>DETAILS</b></th>
    <th><b>DATE OF DATA ACQUISITION</b></th>
	<th><b>DATA FIRST AVAILABLE FOR RESEARCH</b></th>
  </tr>
  <tr>
    <td>Multimodal imaging</td>
    <td>Goal: Imaging available for 100,000 participants. ~40k ready as of early 2020</td>
    <td>  MRI imaging for brain, heart, abdomen and bone densitometry (DXA). <a href="https://www.ukbiobank.ac.uk/imaging-data/">Link to UKB</a></td>
	<td>2014-</td>
	<td>2015</td>
  </tr>
  </table>
   <br/>

### HEALTH RECORD LINKAGE

<table class="others">
  <tr>
    <th></th>
	<th><b>NUMBER OF PARTICIPANTS</b></th>
    <th><b>DETAILS</b></th>
    <th><b>DATE OF DATA ACQUISITION</b></th>
	<th><b>DATA FIRST AVAILABLE FOR RESEARCH</b></th>
  </tr>
  <tr>
    <td>Death registrations</td>
    <td>Whole cohort</td>
    <td><a href="https://biobank.ndph.ox.ac.uk/showcase/field.cgi?id=40001">Primary</a> and <a href="https://biobank.ndph.ox.ac.uk/showcase/field.cgi?id=40002">Secondary</a> ICD-10 coded cause of death. <a href="https://biobank.ndph.ox.ac.uk/showcase/label.cgi?id=100093">Link to UKB.</a></td>
	<td>2006-</td>
	<td>Q2 2013</td>
  </tr>
  <tr>
    <td>Cancer registrations</td>
    <td>Whole cohort</td>
    <td><a href="https://biobank.ndph.ox.ac.uk/showcase/field.cgi?id=40006">ICD coded</a> cancer diagnoses.</td>
	<td>1981-</td>
	<td>Q2 2013</td>
  </tr>
  <tr>
    <td>Hospital inpatient episodes</td>
    <td>Whole cohort</td>
    <td>ICD coded diagnoses. <a href="https://biobank.ndph.ox.ac.uk/showcase/label.cgi?id=2002">Link to UKB.</a></td>
	<td>1997-</td>
	<td>Q2 2013</td>
  </tr>
  <tr>
    <td>Algorithmically-defined outcomes</td>
    <td>Whole cohort</td>
    <td>Health-related events, obtained through algorithmic combinations of coded information from UK Biobank's baseline assessment data collection, linked data from hospital admissions and death registries. <a href="https://biobank.ndph.ox.ac.uk/showcase/label.cgi?id=42">Link to UKB.</a></td>
	<td>2003-</td>
	<td>2015</td>
  </tr>
  <tr>
    <td>Primary care</td>
    <td>~250,000 participants</td>
    <td>Primary care data recorded by health professionals working at general practices. Includes diagnoses, measurements, referrals etc. <a href="https://biobank.ndph.ox.ac.uk/showcase/showcase/docs/primary_care_data.pdf">Link to UKB.</a></td>
	<td>variable</td>
	<td>Q3 2019</td>
  </tr>
  <tr>
    <td>First occurrences</td>
    <td> Whole cohort</td>
    <td>Data shows the 'first occurrence of ~1200 broad health outcomes identified from Primary Care data, Hospital inpatient data, Death Register and self-reported medical condition ICD codes. <a href="https://biobank.ndph.ox.ac.uk/showcase/showcase/docs/first_occurrences_outcomes.pdf">Link to UKB.</a></td>
    <td> variable </td>
    <td> Q3 2019? </td>
  </tr>
  <tr>
    <td>COVID-19</td>
    <td>Whole cohort</td>
    <td> COVID-19 data. Includes COVID-19 tests results, GP clinical events, and prescription records. <a href="https://biobank.ctsu.ox.ac.uk/crystal/exinfo.cgi?src=COVID19_tests">Link to UKB.</a></td>
    <td> Q2 2020 </td>
    <td> Q3 2020 </td>
  </tr>
  </table>
  <br/>

### GENETIC DATA

<table class="others">
  <tr>
    <th></th>
	<th><b>NUMBER OF PARTICIPANTS</b></th>
    <th><b>DETAILS</b></th>
    <th><b>DATE OF DATA ACQUISITION</b></th>
	<th><b>DATA FIRST AVAILABLE FOR RESEARCH</b></th>
  </tr>
  <tr>
    <td>Genotyping (baseline samples)</td>
    <td>Whole cohort</td>
    <td>
		50,000 participants genotyped using the UK BiLEVE array and 100,000 participants   genotyped on the UK Biobank array.
		<a href="https://doi.org/10.1101/166298">Link to QC and imputation details.</a>
	</td>
	<td>2013-2015</td>
	<td>Q3 - 2017</td>
  </tr>
  <tr>
    <td>Exome sequencing</td>
    <td>50,000 exomes available – Whole cohort planned Q4 2020</td>
    <td>
		VCF and CRAM files for 49,960 exomes available. 
		<a href="https://www.ukbiobank.ac.uk/wp-content/uploads/2020/06/UK-Biobank-50k-Exome-Release-FAQ-June-2020.pdf">Link to Exome-seq FAQs</a> Joint-call exome data in pVCF format, sample-level variant (VCFs) and sequence data (CRAMs) for the first 200k exomes planned for November 2020
	</td>
	<td></td>
	<td>Q4 - 2019</td>
  </tr>
  <tr>
    <td>Whole genome sequencing</td>
    <td>Whole cohort planned Q4 2022</td>
    <td>
		For more information on the release of whole genome sequencing data click <a href="https://www.ukbiobank.ac.uk/2019/09/uk-biobank-leads-the-way-in-genetics-research-to-tackle-chronic-diseases/">here.</a>
	</td>
	<td>TBA</td>
	<td>TBA</td>
  </tr>
  </table>
  <br/>

### BIOCHEMICAL DATA

<table class="others">
  <tr>
    <th></th>
	<th><b>NUMBER OF PARTICIPANTS</b></th>
    <th><b>DETAILS</b></th>
    <th><b>DATE OF DATA ACQUISITION</b></th>
	<th><b>DATA FIRST AVAILABLE FOR RESEARCH</b></th>
  </tr>
  <tr>
    <td>Serum biomarker data</td>
    <td>Whole cohort</td>
    <td>
		Urine, packed red blood cells (PRBC) and serum assay data for all participants. 
		<a href="https://biobank.ndph.ox.ac.uk/showcase/showcase/docs/serum_biochemistry.pdf">Link to UKB.</a>
		</td>
	<td>2006-2010 and 2013</td>
	<td>Q1 2019</td>
  </tr>
  </table>
  <br/>

*Source:* Table adapted and updated from [Sudlow et al. 2015](https://dx.doi.org/10.1371%2Fjournal.pmed.1001779) 
